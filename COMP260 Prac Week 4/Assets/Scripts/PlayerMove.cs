﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 5.0f; // in metres/second/second
    public float brake = 5.0f; // in metres/second/second
    public float turnSpeed = 30.0f; // in degrees/second
    public float destroyRadius = 1.0f;
    public string player1_horizontal;
    public string player1_vertical;
    private float speed = 0.0f; // in metres/second
    private BeeSpawner beeSpawner;

    void Start()
    {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(transform.position, destroyRadius);
        }
            // the horizontal axis controls the turn
            float turn = Input.GetAxis(player1_horizontal);

        // turn the car

        //the bug is that the "car" turns while having no speed, which cars can't do

        transform.Rotate(0, 0, turn * (speed * turnSpeed) * Time.deltaTime);
        //now the car turns more quickly/slowly depending on its speed, and its 'reverse' direction
        //while turning is proportional to how a steering wheel would work


        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(player1_vertical);

        //the bug in the code was that the car would accelerate to its maximum speed before slowing down 
        //if the player released the accelerate key, instead of immediately decelerating
        //changing the 'gravity' and 'sensitivy' in the vertical axes input resolved this
        //changing the break to 100.0f did not affect how quickly the player breaks because that would 
        //change the player's speed to a value below or above the clamped value 

        //accelerate/reverse
        speed += (maxSpeed * forwards) * acceleration * Time.deltaTime;

        {
            // braking
            if (speed > 0)
            {
            speed -= brake * Time.deltaTime;
            }
            else if (speed < 0)
            {
            speed += brake * Time.deltaTime;
            }
        }


        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
    }
}