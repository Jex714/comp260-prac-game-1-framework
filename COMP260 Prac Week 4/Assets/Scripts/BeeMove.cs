﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
    //public state
    public float minSpeed = 1;
    public float maxSpeed = 3;
    public float minTurnSpeed = 1;
    public float maxTurnSpeed = 3;
    public Transform target;
    public Transform target1;
    public Transform target2;
    public Player1 player1;
    public Player1 player2;
    public ParticleSystem explosionPrefab;

    // private state
    private Vector2 heading = Vector2.right;
    private float speed;
    private float turnSpeed;

    // Use this for initialization
    void Start ()
    {
        // find the two players
        Player1 player1 = (Player1)
        FindObjectOfType(typeof(Player1));
        Player2 player2 = (Player2)
        FindObjectOfType(typeof(Player2));

        target = player1.transform;
        target1 = player1.transform;
        target2 = player2.transform;

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);
    }

    // Update is called once per frame
    void Update() {

        //determine the closest target
        float dist1 = Vector3.Distance(target1.position, transform.position);
        float dist2 = Vector3.Distance(target2.position, transform.position);

        {
        if (dist1 < dist2)
            {
                target = target1;
            }
        if (dist2 < dist1)
            {
                target = target2;
            }
        }

        // get the vector from the bee to the target
        // and normalise it.
        //Vector2 direction = target.position - transform.position;
        //direction = direction.normalized;
        //Vector2 velocity = direction * speed;
        //transform.Translate(velocity * Time.deltaTime);

        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);
    }
}
